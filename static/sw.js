importScripts('/_nuxt/workbox.4c4f5ca6.js')

workbox.precaching.precacheAndRoute([
  {
    "url": "/_nuxt/36dee4b465201b7f32f1.js",
    "revision": "92c04e43c197e79b236ba3b75d6a39be"
  },
  {
    "url": "/_nuxt/435b03743fd94337409e.js",
    "revision": "72d9e82ba854a154cf5c351213d9060f"
  },
  {
    "url": "/_nuxt/45c273765876f45c2750.js",
    "revision": "66c982ec41ae281ac2b5fc28a9b09715"
  },
  {
    "url": "/_nuxt/58517de16bc9e0911115.js",
    "revision": "a9dc672d220281965d272f60fa5c47fa"
  },
  {
    "url": "/_nuxt/7254e37296d91450911f.js",
    "revision": "e3613be7247375e33940bf8e2c7f8067"
  },
  {
    "url": "/_nuxt/9127ada2112f59cf5644.js",
    "revision": "fbc09e94d410e66b21e56d89cc3c332d"
  },
  {
    "url": "/_nuxt/94cea534464256b5f4fd.js",
    "revision": "726bc506dab152ea8dd3c0c2fee981f9"
  },
  {
    "url": "/_nuxt/a51cc0f88e18b4747966.js",
    "revision": "5cdafbfe1831e36133a5dbe539b1dd12"
  },
  {
    "url": "/_nuxt/a816f8ca4a3c378d935e.js",
    "revision": "85742ccb80ce9b9f4115552d5f00fea2"
  },
  {
    "url": "/_nuxt/a87f96c48669ed777b9f.js",
    "revision": "66abd02b85512492f9462be55c561d94"
  },
  {
    "url": "/_nuxt/bcac48c702cf1cd89302.js",
    "revision": "05e409b8167c3125fe52265510dd170e"
  },
  {
    "url": "/_nuxt/c3c22ef151f392c2c856.js",
    "revision": "837b521209c8c490e594d5c4a2cdb7a8"
  },
  {
    "url": "/_nuxt/c407572b96b8ce843e32.js",
    "revision": "6c78c84d547db0503cefcb7729715b09"
  },
  {
    "url": "/_nuxt/c52a8af910d6716cd444.js",
    "revision": "7b2ab237e8ea823274a2b361b35a8000"
  },
  {
    "url": "/_nuxt/c6dee36aca0c9105f39d.js",
    "revision": "d111da9961dd8eeb89caf01b0d967450"
  },
  {
    "url": "/_nuxt/db0fa2b17cbe5a242361.js",
    "revision": "8046ef92baed997d2d5bb2a438eb9e14"
  },
  {
    "url": "/_nuxt/ead0a3644ad853f9189a.js",
    "revision": "ae1c163c9df5dedfd6e3c72fd99efe11"
  },
  {
    "url": "/_nuxt/f0defbd792a9c86f2b08.js",
    "revision": "89bfa617b1b66b1381fcb8d97714e1d9"
  },
  {
    "url": "/_nuxt/f461840be9ed94b7b5bf.js",
    "revision": "ce35824c178e5ef8c48ad5478abe54e2"
  }
], {
  "cacheId": "uniswapdex",
  "directoryIndex": "/",
  "cleanUrls": false
})

workbox.clientsClaim()
workbox.skipWaiting()

workbox.routing.registerRoute(new RegExp('/_nuxt/.*'), workbox.strategies.cacheFirst({}), 'GET')

workbox.routing.registerRoute(new RegExp('/.*'), workbox.strategies.networkFirst({}), 'GET')
