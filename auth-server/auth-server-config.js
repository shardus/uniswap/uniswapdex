module.exports = {
  environment: 'production',
  staticDirectory: '../dist/_nuxt',
  viewDirectory: '../dist',
  mainRoute: '/',
  port: {
    http: 80,
    https: 443
  },
  keyLocation: '/etc/letsencrypt/live/uniswapdex.com'
}
