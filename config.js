export default {
  uniswapDexServer: 'https://uniswapdex.com:8889/',
  siteKeyServer: 'https://uniswapdex.com/',
  backupInterval: 10 * 60 * 1000,
  version: '1.0.0',
  etherscanApiKey: 'YOUR_ETHERSCAN_KEY',
  recaptchaClientKey: 'YOUR_RECAPTCHA_CLIENT_KEY',
  refreshInterval: 60000
}
